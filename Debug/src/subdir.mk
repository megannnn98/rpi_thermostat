################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/globals.c \
../src/main.c \
../src/thread_buffer_collector.c \
../src/thread_relay.c 

OBJS += \
./src/globals.o \
./src/main.o \
./src/thread_buffer_collector.o \
./src/thread_relay.o 

C_DEPS += \
./src/globals.d \
./src/main.d \
./src/thread_buffer_collector.d \
./src/thread_relay.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -std=c99 -O0 -Wall -c -fPIC -pthread -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


