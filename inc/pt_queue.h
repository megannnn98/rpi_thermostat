/*
 * pt_queue.h
 *
 *  Created on: 26 апр. 2019 г.
 *      Author: b
 */

#ifndef __PT_QUEUE_H__
#define __PT_QUEUE_H__
/*
 * Queues
 */
#define PT_QUEUE(T, size)                                                      \
  struct {                                                                     \
    T buf[size];                                                               \
    unsigned int r;                                                            \
    unsigned int w;                                                            \
  }
#define PT_QUEUE_INIT()                                                        \
  { .r = 0, .w = 0 }
#define PT_QUEUE_LEN(q) (sizeof((q)->buf) / sizeof((q)->buf[0]))
#define PT_QUEUE_CAP(q) ((q)->w - (q)->r)
#define PT_QUEUE_EMPTY(q) ((q)->w == (q)->r)
#define PT_QUEUE_FULL(q) (PT_QUEUE_CAP(q) == PT_QUEUE_LEN(q))
#define PT_QUEUE_RESET(q) ((q)->w = (q)->r = 0)

#define PT_QUEUE_PUSH(q, el)                                                   \
  (!PT_QUEUE_FULL(q) && ((q)->buf[(q)->w++ % PT_QUEUE_LEN(q)] = (el), 1))
#define PT_QUEUE_PEEK(q)                                                       \
  (PT_QUEUE_EMPTY(q) ? NULL : &(q)->buf[(q)->r % PT_QUEUE_LEN(q)])
#define PT_QUEUE_POP(q)                                                        \
  (PT_QUEUE_EMPTY(q) ? NULL : &(q)->buf[(q)->r++ % PT_QUEUE_LEN(q)])

#define PT_QUEUE_PUSH_DEL_OLD(q, el)                                            \
    do { if (PT_QUEUE_FULL(q)) {PT_QUEUE_POP(q); }                       \
        PT_QUEUE_PUSH(q, el);} while(0)
#endif /* __PT_QUEUE_H__ */

