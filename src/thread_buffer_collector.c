
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <pthread.h>
#include "../inc/pt_queue.h"
#include "../inc/utils.h"

extern PT_QUEUE(float, QSIZE) ring;
extern pthread_mutex_t queue_mutex;
extern bool some_data_ready;

/*
static void queue_print()
{
	float pf32;
	static time_t i = 0;
	int cap = PT_QUEUE_CAP(&ring);

	printf("queue[%u]: \n", (unsigned)i++);
	for (int i = 0; i < cap; i++) {
		pf32 = *((float*)PT_QUEUE_POP(&ring));
		printf("[%d] = %.1f\n", i, pf32);
		PT_QUEUE_PUSH_DEL_OLD(&ring, pf32);
	}
	printf("\n");
}*/

static float read_float_from_file(const char* path)
{
	#define MAX_LEN 10
	FILE * fp;
	float f32 = 0;
	char s[MAX_LEN] = {0};

	do {

		sleep(1);
		if ( NULL == (fp = fopen(path, "r"))) {
			printf("\nno file");
			continue;
		}
		break;

	} while(1);

   	fgets(s, MAX_LEN, fp);
   	f32 = atof(s);
	fclose(fp);


	return f32;
}

void* thread_buffer_collector(void* p)
{
	float f32 = 0;
	//time_t i = 0;

	printf("\nthread buffer collector");


	while(1)
	{
		sleep(1);
	   	f32 = read_float_from_file((char*)p);
		pthread_mutex_lock(&queue_mutex);
	   	PT_QUEUE_PUSH_DEL_OLD(&ring, f32);
	   	pthread_mutex_unlock(&queue_mutex);

		continue;
	}
	pthread_exit(NULL);
}
