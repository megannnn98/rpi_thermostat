#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include "../inc/pt_queue.h"

void* thread_relay(void* p);
void* thread_buffer_collector(void* p);

extern pthread_mutex_t queue_mutex;


int main(int argc, char **argv) {

	float target_T;
	int result = 0;
    pthread_t relay_pt, buffer_collector_pt;

	if (argc < 3) {
		argv[1] = "/tmp/t.dat";
		argv[2] = "12.";
	}
	argv[1] = "/tmp/t.dat";
	argv[2] = "12.";

    if (pthread_mutex_init(&queue_mutex, NULL) != 0) {
        printf("\n mutex init has failed\n");
        return 1;
    }

    result = pthread_create(&buffer_collector_pt, NULL, thread_buffer_collector, argv[1]);
    if (result != 0) {
        perror("Creating the buffer_collector_pt thread");
        return EXIT_FAILURE;
    }

	target_T = atof(argv[2]);
    result = pthread_create(&relay_pt, NULL, thread_relay, &target_T);
    if (result != 0) {
        perror("Creating the relay_pt thread");
        return EXIT_FAILURE;
    }


    pthread_join(relay_pt, NULL);
    pthread_join(buffer_collector_pt, NULL);
    pthread_mutex_destroy(&queue_mutex);

	return EXIT_SUCCESS;
}
