/*
 * thread_relay.c
 *
 *  Created on: 26 апр. 2019 г.
 *      Author: b
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <pthread.h>
#include "../inc/pt_queue.h"
#include "../inc/utils.h"

extern PT_QUEUE(float, QSIZE) ring;
extern pthread_mutex_t queue_mutex;

static float queue_average()
{
	float pf32, s = 0;
	int cap = PT_QUEUE_CAP(&ring);
	for (int i = 0; i < cap; i++) {
		pf32 = *((float*)PT_QUEUE_POP(&ring));
		s += pf32;
		PT_QUEUE_PUSH_DEL_OLD(&ring, pf32);
	}

	if (!cap) {
		return 0;
	}

	return s/cap;
}

void* thread_relay(void* p)
{
	float target_T = 0;
	float average = 0;
	time_t i = 0;

	memcpy(&target_T, p, sizeof(float));
	printf("thread relay %.1f\n", target_T);

	while(1)
	{
		sleep(1);
		pthread_mutex_lock(&queue_mutex);
		average = queue_average();
		pthread_mutex_unlock(&queue_mutex);

		printf("\naverage = %.1f -> %.1f\n", average, target_T);
		if (average > target_T) {
			// Вкл реле
			printf("\ncooling[%u]", (unsigned)i++);
		} else {
			// Выкл реле
			printf("\nheating[%u]", (unsigned)i++);

		}


		continue;
	}
	pthread_exit(NULL);
}















